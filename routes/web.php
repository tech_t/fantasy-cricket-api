<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConsumeApi;
use App\Http\Controllers\Admin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/continent', [ConsumeApi::class, 'leagues']);
// Route::get('/seasons', [ConsumeApi::class, 'seasons']);
// Route::get('/teams', [ConsumeApi::class, 'teams']);
// Route::get('/players', [ConsumeApi::class, 'players']);


Route::prefix('admincp')->group(function () {
    Route::get('/login', [Admin::class, 'login']);
    Route::get('/forgot-password', [Admin::class, 'forgot_password']);
    Route::get('/allplayers', [Admin::class, 'allplayers']);

    Route::post('/updatePlayerCredit', [Admin::class, 'updatePlayerCredit']);
    Route::post('/getPlayerEditModal', [Admin::class, 'getPlayerEditModal']);

});

