@include('admin.include.head')
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('admin.include.header')
        <!-- Left side column. contains the logo and sidebar -->
        @include('admin.include.menubar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    All Players
                    <small>All Players List</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">Data tables</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Data Table With Full Features</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($players)){
                                                foreach($players as $player){
                                                    ?>
                                                        <tr>
                                                            <tr>Name</tr>
                                                            <tr>Position</tr>
                                                            <tr>Action</tr>
                                                        </tr>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @include('admin.include.copyright')
        @include('admin.include.right_aside')
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    @include('admin.include.footer')
    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })

            // $('#example1').DataTable( {
            //     "ajax": "{{ url('admincp/getAllPlayersData') }}",
            //     "columns": [
            //         { "data": "name" },
            //         { "data": "position" },
            //         { "data": "office" },
            //         { "data": "extn" },
            //         { "data": "start_date" },
            //         { "data": "salary" }
            //     ]
            // } );
        })
    </script>
</body>

</html>
