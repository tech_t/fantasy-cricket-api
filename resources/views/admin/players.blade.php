@include('admin.include.head')
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('admin.include.header')
        <!-- Left side column. contains the logo and sidebar -->
        @include('admin.include.menubar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    All Players
                    <small>All Players List</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">All Players List</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Data Table With Full Features</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Credit</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($players)){
                                                foreach($players as $player){
                                                    ?>
                                                        <tr>
                                                            <td>{{ $player->fullname }}</td>
                                                            <td>
                                                                <?php
                                                                    if($player->position_id == 1){
                                                                        echo "<span class='badge bg-green'>".strtoupper($player->position)."</span>";
                                                                    }else if($player->position_id == 2){
                                                                        echo "<span class='badge bg-orange'>".strtoupper($player->position)."</span>";
                                                                    }else if($player->position_id == 3){
                                                                        echo "<span class='badge bg-blue'>".strtoupper($player->position)."</span>";
                                                                    }else if($player->position_id == 4){
                                                                        echo "<span class='badge bg-red'>".strtoupper($player->position)."</span>";
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td><input type="text" onkeyup="updatePlayerCredit(this.value,<?= $player->player_id ?>);" value="{{ $player->credit }}"/></td>
                                                            <td>
                                                                <a class="btn btn-info" onclick="editPlayer(<?= $player->player_id ?>);"><i class="fa fa-pencil"></i></a>
                                                                <a class="btn btn-success"><i class="fa fa-eye"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @include('admin.include.copyright')
        @include('admin.include.right_aside')
        @include('admin.modal.player_edit_modal')
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    @include('admin.include.footer')
    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });

        function updatePlayerCredit(valcu,id){
            var formData = {credit:valcu, id:id, _token: '{{csrf_token()}}' };
            $.ajax({
                url : "{{ url('admincp/updatePlayerCredit') }}",
                type: "POST",
                data : formData,
                async : false,
                success: function(response, textStatus, jqXHR) {

                },
            });
        }

        function editPlayer(id){
            var formData = { id: id, _token: '{{csrf_token()}}' };
            $.ajax({
                url : "{{ url('admincp/getPlayerEditModal') }}",
                type: "POST",
                data : formData,
                async : false,
                success: function(response, textStatus, jqXHR) {
                    $("#firstname").val(response.firstname);
                    $("#lastname").val(response.lastname);
                    $("#playerEditModal").modal('show')
                },
            });
        }
    </script>
</body>

</html>
