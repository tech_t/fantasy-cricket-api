<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;

class Admin extends Controller
{
    public function __construct(){

    }

    public function login(){
        return view('admin.login');
    }

    public function forgot_password(){
        return view('admin.forgot_password');
    }

    public function allplayers(){
        $result['players'] = DB::table('tbl_players as P')->join('tbl_countries as C','C.cont_id','=','P.country_id')->get();
        return view('admin.players')->with($result);
    }

    public function updatePlayerCredit(Request $request){
        $update = [
            'credit' => $request->credit,
        ];
        DB::table('tbl_players')->where(['player_id'=>$request->id])->update($update);
    }


    public function getPlayerEditModal(Request $request){
        $result = DB::table('tbl_players')->where(['player_id'=>$request->id])->first();
        return response()->json($result);
    }

}
