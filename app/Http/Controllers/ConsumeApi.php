<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ConsumeApi extends Controller
{
    public function __construct(){

    }

    public function leagues(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://cricket.sportmonks.com/api/v2.0/leagues?api_token=knAhZgO0JA2IoPEauYMAcp9b6cHvkiA1w8Pxh64IiBEnNDY7QuaVvB2RAIg3',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        echo "<pre>"; print_r($array);
        foreach($array->data as $country){
            $file = $country->image_path;
            if($this->checkUrl($file)){
                $country->name = str_replace('/','-',$country->name);
                $newfile = base_path().'/leage_flag/'.$country->name.".png";
                if(copy($file, $newfile)){
                    $newfile = '/leage_flag/'.$country->name.".png";
                }else{
                    $newfile = '/leage_flag/noimg.png';
                }
            }
            $insert = [
                'season_id' => $country->season_id,
                'country_id' => $country->country_id,
                'image_path'  => $newfile,
                'code' => $country->code,
                'name' => $country->name,
            ];
            DB::table('tbl_leagues')->insert($insert);
        }

    }

    public function country(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://cricket.sportmonks.com/api/v2.0/countries?api_token=knAhZgO0JA2IoPEauYMAcp9b6cHvkiA1w8Pxh64IiBEnNDY7QuaVvB2RAIg3',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        echo "<pre>"; print_r($array);
        foreach($array->data as $country){
            $file = $country->image_path;
            if($this->checkUrl($file)){
                $country->name = str_replace('/','-',$country->name);
                $newfile = base_path().'/leage_flag/'.$country->name.".png";
                if(copy($file, $newfile)){
                    $newfile = '/leage_flag/'.$country->name.".png";
                }else{
                    $newfile = '/leage_flag/noimg.png';
                }
            }
            $insert = [
                'cont_id' => $country->id,
                'name' => $country->name,
                'img'  => $newfile,
            ];
            DB::table('tbl_countries')->insert($insert);
        }
    }

    function checkUrl($file){
        $file_headers = @get_headers($file);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        }
        else {
            $exists = true;
        }
        return $exists;
    }

    function validImage($file) {
        $size = getimagesize($file);
        return (strtolower(substr($size['mime'], 0, 5)) == 'image' ? true : false);  
     }

    function seasons(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://cricket.sportmonks.com/api/v2.0/seasons?api_token=knAhZgO0JA2IoPEauYMAcp9b6cHvkiA1w8Pxh64IiBEnNDY7QuaVvB2RAIg3',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        echo "<pre>"; print_r($array);
        foreach($array->data as $country){
            $insert = [
                'seasons_id' => $country->id,
                'league_id' => $country->league_id,
                'name' => $country->name,
                'code' => $country->code,
            ];
            DB::table('tbl_seasons')->insert($insert);
        }
    }

    public function teams(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://cricket.sportmonks.com/api/v2.0/teams?api_token=knAhZgO0JA2IoPEauYMAcp9b6cHvkiA1w8Pxh64IiBEnNDY7QuaVvB2RAIg3',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        echo "<pre>"; print_r($array);
        foreach($array->data as $country){
            $file = $country->image_path;
            if($this->checkUrl($file)){
                $country->name = str_replace('/','-',$country->name);
                $newfile = base_path().'/team_flag/'.$country->name.".png";
                if(copy($file, $newfile)){
                    $newfile = '/team_flag/'.$country->name.".png";
                }else{
                    $newfile = '/team_flag/noimg.png';
                }
            }
            $insert = [
                'country_id' => $country->country_id,
                'name' => $country->name,
                'image_path'  => $newfile,
                'team_id' => $country->id,
                'national_team' => ($country->id) ? 1 : 0,
                'code'  =>  $country->code,
            ];
            DB::table('tbl_teams')->insert($insert);
        }
    }

    public function players(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://cricket.sportmonks.com/api/v2.0/players?api_token=knAhZgO0JA2IoPEauYMAcp9b6cHvkiA1w8Pxh64IiBEnNDY7QuaVvB2RAIg3',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        echo "<pre>"; print_r($array);
        foreach($array->data as $country){
            //$file = $country->image_path;
            // if($this->checkUrl($file) && $this->validImage($file)){
            //     $fileName = str_replace(' ','_',$country->fullname);
            //     $newfile = base_path().'/player_img/'.$fileName.".png";
            //     if(copy($file, $newfile)){
            //         $newfile = '/player_img/'.$fileName.".png";
            //     }else{
            //         $newfile = '/player_img/noimg.png';
            //     }
            // }
            $insert = [
                'player_id' => $country->id,
                'country_id' => $country->country_id,
                'firstname' => $country->firstname,
                'lastname' => $country->lastname,
                'fullname' => $country->fullname,
                'image_path' => $newfile ?? '',
                'dateofbirth' => $country->dateofbirth,
                'gender' => $country->gender,
                'battingstyle' => $country->battingstyle,
                'bowlingstyle'  => $country->bowlingstyle,
                'position_id' => $country->position->id,
                'position' =>  $country->position->name,
            ];
            DB::table('tbl_players')->insert($insert);
        }
    }
}
